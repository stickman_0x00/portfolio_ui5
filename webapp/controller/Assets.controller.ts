import Dialog from "sap/m/Dialog";
import BaseController from "./BaseController";
import Button, { Button$PressEvent } from "sap/m/Button";
import JSONModel from "sap/ui/model/json/JSONModel";
import { Asset } from "../types/portfolioAPI";
import validations from "../model/validations";
import Input from "sap/m/Input";
import MessageToast from "sap/m/MessageToast";
import MessageBox from "sap/m/MessageBox";
import ColumnListItem from "sap/m/ColumnListItem";
import ComboBox, { ComboBox$SelectionChangeEvent } from "sap/m/ComboBox";
import { ListBase$DeleteEvent } from "sap/m/ListBase";

/**
 * @namespace com.gitlab.stickman_0x00.portfolio_ui5.controller
 */
export default class Assets extends BaseController {
	dialogAsset: Dialog;

	onRefresh() {
		void this.getAssetsModel().refresh();
	}

	async onAddAsset() {
		if (!this.dialogAsset) {
			this.dialogAsset = await this.loadFragment({
				name: "com.gitlab.stickman_0x00.portfolio_ui5.view.dialog.Asset"
			}) as Dialog;
			this.dialogAsset.setTitle("New Asset");
			this.dialogAsset.setModel(new JSONModel({} as Asset));
			this.dialogAsset.setEndButton(new Button({
				text: "Create",
				press: this.onDialogAssetCreate.bind(this),
				type: "Accept"
			}));
		}
		this.dialogAsset.open();
	}

	onDialogAssetCreate() {
		const inputs = [
			this.byId("dialogAssetName") as Input,
			this.byId("dialogAssetCode") as Input,
			this.byId("dialogAssetSymbol") as Input
		];
		if (!validations.isFieldsValid(inputs)) {
			return;
		}

		const asset = (this.dialogAsset.getModel() as JSONModel).getData() as Asset;

		this.getAssetsModel().create(asset)
			.then((asset: Asset) => {
				this.onDialogAssetClose();
				MessageToast.show(`Asset ${asset.Name} created`);
			})
			.catch((error: Error) => {
				MessageBox.error(error.message);
			});
	}

	async onAssetEdit(oEvent: Button$PressEvent) {
		const button = oEvent.getSource();
		const item = button.getParent() as ColumnListItem;
		const assetId = item.getBindingContext("assets").getProperty("Id") as number;

		const asset = await this.getAssetsModel().get(assetId);

		if (!this.dialogAsset) {
			this.dialogAsset = await this.loadFragment({
				name: "com.gitlab.stickman_0x00.portfolio_ui5.view.dialog.Asset"
			}) as Dialog;
			this.dialogAsset.setTitle(`Update asset ${asset.Id}`);
			this.dialogAsset.setModel(new JSONModel(asset));
			this.dialogAsset.setEndButton(new Button({
				text: "Update",
				press: this.onDialogAssetUpdate.bind(this),
				type: "Accept"
			}));
		}
		this.dialogAsset.open();
	}

	onDialogAssetUpdate() {
		const inputs = [
			this.byId("dialogAssetName") as Input,
			this.byId("dialogAssetCode") as Input,
			this.byId("dialogAssetSymbol") as Input
		];
		if (!validations.isFieldsValid(inputs)) {
			return;
		}

		const asset = (this.dialogAsset.getModel() as JSONModel).getData() as Asset;

		this.getAssetsModel().update(asset.Id, asset)
			.then((asset: Asset) => {
				this.onDialogAssetClose();
				MessageToast.show(`Asset ${asset.Name} updated`);
			})
			.catch((error: Error) => {
				MessageBox.error(error.message);
			});
	}

	onDialogAssetClose() {
		this.dialogAsset.close();
	}

	onDialogAssetAfterClose() {
		this.dialogAsset.destroy();
		this.dialogAsset = null;
	}

	onDialogAssetSourceSelectionChange(oEvent: ComboBox$SelectionChangeEvent) {
		const comboBox = oEvent.getParameter("selectedItem");
		if (!comboBox) {
			return;
		}

		this.getSourcesModel().getAssets(comboBox.getKey())
			.then((sourceAssets: Asset[]) => {
				const model = new JSONModel(sourceAssets);
				model.setSizeLimit(sourceAssets.length)

				this.getView().setModel(model, "sourceAssets");
			})
			.catch((error: Error) => {
				console.error(error);
			})
	}

	onAssetDelete(oEvent: ListBase$DeleteEvent) {
		const item = oEvent.getParameter("listItem");
		const assetId = item.getBindingContext("assets").getProperty("Id") as number;

		MessageBox.confirm("Are you sure you want to delete this asset?", {
			actions: [MessageBox.Action.CANCEL, MessageBox.Action.DELETE],
			emphasizedAction: MessageBox.Action.DELETE,
			onClose: (oAction: keyof typeof MessageBox.Action) => {
				if (oAction === MessageBox.Action.DELETE) {
					this.getAssetsModel().delete(assetId)
						.catch((error: Error) => {
							MessageBox.error(error.message);
						});
				}
			}
		});
	}
}
