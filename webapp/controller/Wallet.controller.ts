import { Route$MatchedEvent } from "sap/ui/core/routing/Route";
import BaseController from "./BaseController";
import { Wallet as WalletType } from "../types/portfolioAPI";
import MessageBox from "sap/m/MessageBox";
import Dialog from "sap/m/Dialog";
import JSONModel from "sap/ui/model/json/JSONModel";
import Button from "sap/m/Button";
import MessageToast from "sap/m/MessageToast";
import Input from "sap/m/Input";
import validations from "../model/validations";

/**
 * @namespace com.gitlab.stickman_0x00.portfolio_ui5.controller
 */
export default class Wallet extends BaseController {
	private walletId: int;
	private dialogWallet: Dialog;

	onInit(): void {
		const oRouter = this.getRouter();
		oRouter.getRoute("wallet").attachMatched(this.onRouteMatched, this);
	}

	onRouteMatched = (oEvent: Route$MatchedEvent) => {
		const oArgs = oEvent.getParameter("arguments") as { Id: string };
		this.walletId = parseInt(oArgs.Id);

		this.loadWalletData();
	}

	loadWalletData() {
		this.getWalletsModel().get(this.walletId)
			.then((wallet: WalletType) => {
				this.getView().setModel(new JSONModel(wallet));
			})
			.catch(() => {
				void this.getWalletsModel().refresh();
				this.onNavBack();
			});
	}

	onDeleteWallet() {
		MessageBox.confirm("Are you sure you want to delete this wallet?", {
			actions: [MessageBox.Action.CANCEL, MessageBox.Action.DELETE],
			emphasizedAction: MessageBox.Action.DELETE,
			onClose: (oAction: keyof typeof MessageBox.Action) => {
				if (oAction === MessageBox.Action.DELETE) {
					this.getWalletsModel().delete(this.walletId)
						.then(() => {
							this.onNavBack();
						})
						.catch((error: Error) => {
							MessageBox.error(error.message);
						});
				}
			}
		});
	}

	async onEditWallet() {
		if (!this.dialogWallet) {
			this.dialogWallet = await this.loadFragment({
				name: "com.gitlab.stickman_0x00.portfolio_ui5.view.dialog.Wallet"
			}) as Dialog;
			const wallet = (this.getView().getModel() as JSONModel).getData() as WalletType;
			this.dialogWallet.setTitle(`Update wallet ${wallet.Id}`);
			this.dialogWallet.setModel(new JSONModel(wallet));
			this.dialogWallet.setEndButton(new Button({
				text: "Update",
				press: this.onDialogWalletUpdate.bind(this),
				type: "Accept"
			}));
		}
		this.dialogWallet.open();
	}

	onDialogWalletUpdate() {
		const inputs = [this.byId("dialogWalletName") as Input];
		if (!validations.isFieldsValid(inputs)) {
			return;
		}

		const wallet = (this.dialogWallet.getModel() as JSONModel).getData() as WalletType;

		this.getWalletsModel().update(this.walletId, wallet)
			.then((wallet) => {
				this.onDialogWalletClose();
				(this.getView().getModel() as JSONModel).setData(wallet);
				MessageToast.show(`Wallet updated successfully!`);
			})
			.catch((error: Error) => {
				MessageBox.error(error.message);
			})
	}

	onDialogWalletClose() {
		this.dialogWallet.close();
	}

	onDialogWalletgAfterClose() {
		this.dialogWallet.destroy();
		this.dialogWallet = null;
	}
}
