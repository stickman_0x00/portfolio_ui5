import { Route$PatternMatchedEvent } from "sap/ui/core/routing/Route";
import BaseController from "./BaseController";
import JSONModel from "sap/ui/model/json/JSONModel";
import Dialog from "sap/m/Dialog";
import Button, { Button$PressEvent } from "sap/m/Button";
import { Transaction, TransactionProperty } from "../types/portfolioAPI";
import validations from "../model/validations";
import MessageToast from "sap/m/MessageToast";
import MessageBox from "sap/m/MessageBox";
import FlexBox from "sap/m/FlexBox";
import Table from "sap/m/Table";
import Select, { Select$ChangeEvent } from "sap/m/Select";
import { DateTimePicker$ChangeEvent } from "sap/ui/webc/main/DateTimePicker";
import formatter from "../model/formatter";
import ColumnListItem from "sap/m/ColumnListItem";
import Input from "sap/m/Input";
import ComboBox from "sap/m/ComboBox";
import { ListBase$DeleteEvent } from "sap/m/ListBase";
import { ListItemBase$PressEvent } from "sap/m/ListItemBase";

/**
 * @namespace com.gitlab.stickman_0x00.portfolio_ui5.controller
 */
export default class Transactions extends BaseController {
	private walletId: int;
	private dialogTransaction: Dialog;

	onInit(): void {
		const oRouter = this.getRouter();
		oRouter.getRoute("wallet").attachMatched(this.onRouteMatched, this);
	}

	onRouteMatched = (oEvent: Route$PatternMatchedEvent) => {
		const oArgs = oEvent.getParameter("arguments") as { Id: string };
		this.walletId = parseInt(oArgs.Id);

		// Set transactions model
		this.getView().setModel(new JSONModel());
		this.getTransactionsModel().setModel(this.getView().getModel() as JSONModel);
		this.getTransactionsModel().setWallet(this.walletId);
		this.onRefreshTransactions();
	}

	onRefreshTransactions() {
		void this.getTransactionsModel().refresh();
	}

	async onAddTransaction() {
		if (!this.dialogTransaction) {
			this.dialogTransaction = await this.loadFragment({
				name: "com.gitlab.stickman_0x00.portfolio_ui5.view.dialog.Transaction"
			}) as Dialog;
			this.dialogTransaction.setTitle("New Transaction");
			this.dialogTransaction.setModel(new JSONModel({
				Wallet: this.walletId,

				Deposits: [],
				Earnings: [],
				Withdraws: [],
				Trades: [],
				Transfers: [],

				Fees: [],
			} as Transaction));
			this.dialogTransaction.setEndButton(new Button({
				text: "Create",
				press: this.onDialogTransactionCreate.bind(this),
				type: "Accept"
			}));
		}
		this.dialogTransaction.open();
	}

	dialogTransactionTimeChange(oEvent: DateTimePicker$ChangeEvent) {
		const transactionModel = (this.dialogTransaction.getModel() as JSONModel);
		const value = oEvent.getParameter("value");
		transactionModel.setProperty("/Time", formatter.toUnix(value));
	}

	async onDialogTransactionTypeChange(oEvent: Select$ChangeEvent) {
		const type = oEvent.getParameter("selectedItem").getText();
		const flexBox = this.byId("dialogTransactionFlexBox") as FlexBox;
		const table = await this.loadFragment({
			name: `com.gitlab.stickman_0x00.portfolio_ui5.view.dialog.transaction.${type}`
		}) as Table;
		flexBox.removeAllItems();
		flexBox.addItem(table);
	}

	onDialogTransactionAdd(oEvent: Button$PressEvent) {
		const transaction = (this.dialogTransaction.getModel() as JSONModel).getData() as Transaction;
		let property: TransactionProperty[];
		if (oEvent.getSource().getId().includes("feeAddButton")) {
			property = transaction.Fees;
		} else {
			const select = this.byId("dialogTransactionType") as Select;
			const type = select.getSelectedItem().getText() + "s" as keyof Transaction;
			property = transaction[type] as TransactionProperty[];
		}
		property.push({} as TransactionProperty);
		this.dialogTransaction.getModel().refresh();
	}

	onDialogTransactionCreate() {
		const inputs: (Input | Select | ComboBox)[] = [
			// this.byId("dialogTransactionTime") as DateTimePicker,
			this.byId("dialogTransactionType") as Select,
		];
		if (!validations.isFieldsValid(inputs)) return;
		inputs.pop();

		const flexBox = this.byId("dialogTransactionFlexBox") as FlexBox;
		const table = flexBox.getItems()[0] as Table;
		const rows = table.getItems() as ColumnListItem[];
		rows.forEach((row) => {
			(row.getCells() as (Input | Select | ComboBox)[]).forEach((cell) => {
				inputs.push(cell);
			});
		});

		if (!validations.isFieldsValid(inputs)) return;

		if (!rows.length) {
			MessageToast.show("Empty table.");
			return;
		}

		inputs.splice(0, inputs.length);
		const tableFee = this.byId("tableFees") as Table;
		const rowsFee = tableFee.getItems() as ColumnListItem[];

		if (rowsFee.length) {
			rowsFee.forEach((row) => {
				(row.getCells() as (Input | Select)[]).forEach((cell) => {
					inputs.push(cell);
				});
			});
		}
		if (!validations.isFieldsValid(inputs)) return;

		const transaction = (this.dialogTransaction.getModel() as JSONModel).getData() as Transaction;

		this.getTransactionsModel().create(transaction)
			.then((transaction: Transaction) => {
				this.onDialogTransactionClose();
				MessageToast.show(`Transaction ${transaction.Id} added!`);
			})
			.catch((error: Error) => {
				MessageBox.error(error.message);
			})
	}

	async onTransactionPress(oEvent: ListItemBase$PressEvent) {
		const item = oEvent.getSource();
		let transaction = item.getBindingContext().getObject() as Transaction;

		if (this.dialogTransaction) {
			this.onDialogTransactionAfterClose();
		}

		this.dialogTransaction = await this.loadFragment({
			name: "com.gitlab.stickman_0x00.portfolio_ui5.view.dialog.Transaction"
		}) as Dialog;
		this.dialogTransaction.setTitle(`Transaction ${transaction.Id}`);

		transaction = await this.getTransactionsModel().get(transaction.Id);

		this.dialogTransaction.setModel(new JSONModel(transaction));
		(this.dialogTransaction.getModel() as JSONModel).setProperty("/editable", false);

		const select = this.byId("dialogTransactionType") as Select;
		select.fireChange({ selectedItem: select.getSelectedItem() });
		this.dialogTransaction.open();
	}

	onDialogTransactionTableDelete(oEvent: ListBase$DeleteEvent) {
		const table = oEvent.getSource();
		const transaction = (this.dialogTransaction.getModel() as JSONModel).getData() as Transaction;
		let property: TransactionProperty[];
		if (table.getId().includes("tableFees")) {
			property = transaction.Fees;
		} else {
			const select = this.byId("dialogTransactionType") as Select;
			const type = select.getSelectedItem().getText() + "s" as keyof Transaction;
			property = transaction[type] as TransactionProperty[];
		}

		const item = oEvent.getParameter("listItem") as ColumnListItem;
		const index = table.indexOfItem(item);

		property.splice(index, 1);
		this.dialogTransaction.getModel().refresh();
	}

	onDialogTransactionClose() {
		this.dialogTransaction.close();
	}

	onDialogTransactionAfterClose() {
		this.dialogTransaction.destroy();
		this.dialogTransaction = null;
	}
}
