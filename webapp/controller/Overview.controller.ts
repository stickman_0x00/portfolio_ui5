import Wallet from "./Wallet.controller";

/**
 * @namespace com.gitlab.stickman_0x00.portfolio_ui5.controller
 */
export default class Overview extends Wallet {
	onRefreshWallet(): void {
		this.loadWalletData();
	}
}
