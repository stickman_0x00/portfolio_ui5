
import { ListItemBase$PressEvent } from "sap/m/ListItemBase";
import BaseController from "./BaseController";
import { Wallet } from "../types/portfolioAPI";
import Dialog from "sap/m/Dialog";
import Input from "sap/m/Input";
import validations from "../model/validations";
import MessageBox from "sap/m/MessageBox";
import MessageToast from "sap/m/MessageToast";
import JSONModel from "sap/ui/model/json/JSONModel";
import Button from "sap/m/Button";
/**
 * @namespace com.gitlab.stickman_0x00.portfolio_ui5.controller
 */
export default class Wallets extends BaseController {
	private dialogWallet: Dialog;

	onWalletPress(oEvent: ListItemBase$PressEvent) {
		const oItem = oEvent.getSource();
		const walletId = (oItem.getBindingContext("wallets").getObject() as Wallet).Id;
		this.getRouter().navTo("wallet", {
			Id: walletId
		});
	}

	onAssets() {
		this.getRouter().navTo("assets");
	}

	onRefresh() {
		void this.getWalletsModel().refresh();
		void this.getAssetsModel().refresh();
		void this.getTransactionTypesModel().refresh();
		void this.getSourcesModel().refresh();
	}

	async onAddWallet() {
		if (!this.dialogWallet) {
			this.dialogWallet = await this.loadFragment({
				name: "com.gitlab.stickman_0x00.portfolio_ui5.view.dialog.Wallet"
			}) as Dialog;
			this.dialogWallet.setTitle("New Wallet");
			this.dialogWallet.setModel(new JSONModel({} as Wallet));
			this.dialogWallet.setEndButton(new Button({
				text: "Create",
				press: this.onDialogWalletCreate.bind(this),
				type: "Accept"
			}));
		}
		this.dialogWallet.open();
	}

	onDialogWalletCreate() {
		const inputs = [this.byId("dialogWalletName") as Input];
		if (!validations.isFieldsValid(inputs)) {
			return;
		}

		const wallet = (this.dialogWallet.getModel() as JSONModel).getData() as Wallet;

		this.getWalletsModel().create(wallet)
			.then((wallet: Wallet) => {
				this.onDialogWalletClose();
				MessageToast.show(`Wallet "${wallet.Name}" added`);
			})
			.catch((error: Error) => {
				MessageBox.error(error.message);
			})
	}

	onDialogWalletClose() {
		this.dialogWallet.close();
	}

	onDialogWalletgAfterClose() {
		this.dialogWallet.destroy();
		this.dialogWallet = null;
	}

}
