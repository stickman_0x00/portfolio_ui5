import BaseController from "./BaseController";

/**
 * @namespace com.gitlab.stickman_0x00.portfolio_ui5.controller
 */
export default class App extends BaseController {
	public onInit(): void {
		// apply content density mode to root view
		this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
	}
}
