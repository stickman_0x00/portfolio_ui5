import DateFormat from "sap/ui/core/format/DateFormat";
import { Asset, Type } from "../types/portfolioAPI";

export default {
	toDate: (value: number) => {
		if (!value) {
			return null;
		}

		const date = new Date(value * 1000);
		const dateFormat = DateFormat.getDateTimeInstance();
		return dateFormat.format(date);
	},

	toUnix: (value: string) => {
		if (!value) {
			return null;
		}

		const date = new Date(value);
		return Math.floor(date.getTime() / 1000);
	},

	typeName: (value: number, types: Type[]) => {

		const oType = types.find((oType: Type) => oType.Id === value);
		return oType ? oType.Name : "";
	},

	assetSymbol: (value: number, assets: Asset[]) => {
		const asset = assets.find((asset: Asset) => asset.Id === value);
		return asset ? asset.Symbol : "";
	},

	assetCode: (value: number, assets: Asset[]) => {
		const asset = assets.find((asset: Asset) => asset.Id === value);
		return asset ? asset.Code : "";
	},

	amountState: (value: number) => {
		if (value == 0) {
			return "None";
		}

		return value > 0 ? "Success" : "Error";
	}

};
