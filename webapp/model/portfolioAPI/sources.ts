import JSONModel from "sap/ui/model/json/JSONModel";
import Component from "../../Component";
import EndPoint from "./endpoint";
import { Asset } from "../../types/portfolioAPI";

class Sources extends EndPoint<string[]> {
	constructor(component: Component, urlPortfolio: string) {
		super("sources", `${urlPortfolio}/sources`, component.getModel("sources") as JSONModel);
	}

	async getAssets(source: string): Promise<Asset[]> {
		const response = await fetch(`${this.url}/${source}/assets`);

		if (!response.ok) {
			const error = await response.text()
			throw new Error(`Failed to fetch assets from source: ${error}`);
		}

		return response.json() as Promise<Asset[]>;
	}
}

export default Sources;
