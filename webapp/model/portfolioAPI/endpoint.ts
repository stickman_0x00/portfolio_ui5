import JSONModel from 'sap/ui/model/json/JSONModel';

interface IEndPoint<T> {
	setModel(model: JSONModel): void;
	refresh(): Promise<void>;
	get(id: int): Promise<T>;
	create(obj: T): Promise<T>;
	update(id: int, obj: T): Promise<T>;
	delete(id: int): Promise<void>;
	getData(): T[];
}


export default class EndPoint<T> implements IEndPoint<T> {
	protected model: JSONModel;
	protected url: string;
	protected type: string;

	constructor(type: string, url: string, model: JSONModel) {
		this.type = type;
		this.url = url;
		this.model = model;
	}

	setModel(model: JSONModel): void {
		this.model = model;
	}

	async refresh(): Promise<void> {
		const response = await fetch(this.url);

		if (!response.ok) {
			const error = await response.text()
			throw new Error(`Failed to fetch ${this.type}s: ${error}`)
		}

		const data = await response.json() as T[];
		this.model.setData(data);
	}

	async get(id: int): Promise<T> {
		const response = await fetch(`${this.url}/${id}`);

		if (!response.ok) {
			const error = await response.text()
			throw new Error(`Failed to fetch ${this.type}: ${error}`);
		}

		return response.json() as Promise<T>;
	}

	async create(obj: T): Promise<T> {
		const response = await fetch(this.url, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify(obj)
		});

		if (!response.ok) {
			const error = await response.text()
			throw new Error(`Failed to add ${this.type}: ${error}`);
		}

		void this.refresh();

		return response.json() as Promise<T>;
	}

	async update(id: int, obj: T): Promise<T> {
		const response = await fetch(`${this.url}/${id}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify(obj)
		});

		if (!response.ok) {
			const error = await response.text()
			throw new Error(`Failed to update ${this.type}: ${error}`);
		}

		void this.refresh();

		return response.json() as Promise<T>;
	}

	async delete(id: int): Promise<void> {
		const response = await fetch(`${this.url}/${id}`, {
			method: "DELETE"
		});

		if (!response.ok) {
			const error = await response.text()
			throw new Error(`Failed to delete ${this.type}: ${error}`);
		}

		void this.refresh();
	}

	getData() {
		return this.model.getData() as T[];
	}
}
