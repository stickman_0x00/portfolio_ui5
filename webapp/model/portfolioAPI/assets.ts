import JSONModel from "sap/ui/model/json/JSONModel";
import { Asset } from "../../types/portfolioAPI";
import Component from "../../Component";
import EndPoint from "./endpoint";

class Assets extends EndPoint<Asset> {
	constructor(component: Component, urlPortfolio: string) {
		super("asset", `${urlPortfolio}/assets`, component.getModel("assets") as JSONModel);
	}
}

export default Assets;
