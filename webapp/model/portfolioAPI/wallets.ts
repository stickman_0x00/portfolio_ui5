import { Wallet } from "../../types/portfolioAPI";
import JSONModel from "sap/ui/model/json/JSONModel";
import Component from "../../Component";
import EndPoint from "./endpoint";

class Wallets extends EndPoint<Wallet> {
	constructor(component: Component, urlPortfolio: string) {
		super("wallet", `${urlPortfolio}/wallets`, component.getModel("wallets") as JSONModel);
	}
}

export default Wallets;
