import { Transaction } from "../../types/portfolioAPI";
import EndPoint from "./endpoint";

class Transactions extends EndPoint<Transaction> {
	private walletId: number;

	constructor(urlPortfolio: string) {
		super("transaction", `${urlPortfolio}/transactions`, null);
	}

	setWallet(walletId: number) {
		this.walletId = walletId;
	}

	async refresh(): Promise<void> {
		const response = await fetch(`${this.url}?walletId=${this.walletId}`);

		if (!response.ok) {
			const error = await response.text()
			throw new Error(`Failed to fetch ${this.type}s: ${error}`)
		}

		const data = await response.json() as Transaction[];
		this.model.setData(data);
	}
}

export default Transactions;
