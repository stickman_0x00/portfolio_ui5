import JSONModel from "sap/ui/model/json/JSONModel";
import { TransactionType } from "../../types/portfolioAPI";
import Component from "../../Component";
import EndPoint from "./endpoint";

class TransactionTypes extends EndPoint<TransactionType> {
	constructor(component: Component, urlPortfolio: string) {
		super("transactionType", `${urlPortfolio}/transactionTypes`, component.getModel("transactionTypes") as JSONModel);
	}
}

export default TransactionTypes;
