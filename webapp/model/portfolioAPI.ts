import Component from "../Component";
import Wallets from "./portfolioAPI/wallets";
import Assets from "./portfolioAPI/assets";
import Transactions from "./portfolioAPI/transactions";
import TransactionTypes from "./portfolioAPI/transactionTypes";
import Sources from "./portfolioAPI/sources";

const urlPortfolio = "http://localhost:8000";


class portfolioAPI {
	walletsModel: Wallets;
	assetsModel: Assets;
	transactionsModel: Transactions;
	transactionTypesModel: TransactionTypes;
	sourcesModel: Sources;

	constructor(component: Component) {
		this.walletsModel = new Wallets(component, urlPortfolio);
		this.assetsModel = new Assets(component, urlPortfolio);
		this.transactionsModel = new Transactions(urlPortfolio);
		this.transactionTypesModel = new TransactionTypes(component, urlPortfolio);
		this.sourcesModel = new Sources(component, urlPortfolio);
	}
}

export default portfolioAPI;
