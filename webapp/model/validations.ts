import ComboBox from "sap/m/ComboBox";
import DateTimePicker from "sap/m/DateTimePicker";
import Input from "sap/m/Input";
import Select from "sap/m/Select";

export default {
	isFieldsValid: (fields: (Input | Select | DateTimePicker | ComboBox)[]) => {
		let isValid = true;

		fields.forEach((field) => {
			field.setValueState("None");
			switch (field.getMetadata().getName()) {
				case Input.getMetadata().getName():
					field = field as Input;
					if (!field.getValue()) {
						field.setValueState("Error");
						isValid = false;
					}

					return;
				case Select.getMetadata().getName():
					field = field as Select;
					if (!field.getSelectedKey()) {
						field.setValueState("Error");
						isValid = false;
					}

					return;
				case DateTimePicker.getMetadata().getName():
					field = field as DateTimePicker;
					if (!field.getDateValue()) {
						field.setValueState("Error");
						isValid = false;
					}

					return;
				case ComboBox.getMetadata().getName():
					field = field as ComboBox;
					if (field.getValue() && !field.getSelectedKey()) {
						field.setValueState("Error");
						isValid = false;
					}

					return;
			}
		});


		return isValid;
	}
}
