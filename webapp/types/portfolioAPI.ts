export type TotalAmount = {
	Amount: number;
	Asset: number;
}

export type Wallet = {
	Id: number;
	Name: string;
	Asset: number;
	Assets: TotalAmount[];
}

export type Asset = {
	Id: number;
	Name: string;
	Code: string;
	Symbol: string;
	Source: string;
	SourceAsset: string;
}

export type Deposit = {
	Id: number;
	Amount: number;
	Asset: Asset;
}

export type Earning = {
	Id: number;
	Amount: number;
	Asset: Asset;
}

export type Withdraw = {
	Id: number;
	Amount: number;
	Asset: Asset;
}

export type Trade = {
	Id: number;
	BuyAmount: number;
	BuyAsset: Asset;
	SellAmount: number;
	SellAsset: Asset;
}

export type Transfer = {
	Id: number;
	Amount: number;
	Asset: Asset;
	Receiver: Wallet;
	Sender: Wallet;
}

export type Fee = {
	Id: number;
	Amount: number;
	Asset: Asset;
}

export type Type = {
	Id: number;
	Name: string;
}

export type TransactionType = {
	Id: number;
	Name: string;
}

export type TransactionProperty = (Deposit | Earning | Withdraw | Trade | Transfer | Fee);

export type Transaction = {
	Id: number;
	Time: number;
	Type: number;
	Wallet: number;

	Deposits: Deposit[];
	Earnings: Earning[];
	Withdraws: Withdraw[];
	Trades: Trade[];
	Transfers: Transfer[];

	Fees: Fee[];

	TotalAmount: TotalAmount[];
}
